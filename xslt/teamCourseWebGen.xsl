<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs"
    version="2.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
        scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 10, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> Trainor</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>

    <xsl:output
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        method="xml" indent="yes"/>

    <xsl:template match="/">

        <xsl:call-template name="generate_index_page"/>
        <xsl:call-template name="generate_individual_pages"/>

    </xsl:template>

    <xsl:template name="generate_index_page">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title> Teams Index </title>
                <meta http-equiv="pragma" content="no-cache"/>
                <meta http-equiv="expires" content="-1"/>
                <meta http-equiv="cache-control" content="no-cache"/>
                <meta http-equiv="content-type"
                    content="text/html; charset=utf-8"/>
                <link rel="stylesheet" type="text/css"
                    href="css/course_info.css"/>
            </head>
            <body onunload="">

                <xsl:call-template name="common_title_div"/>

                <div class="navigation">
                    <p>
                        <a href="index.html">Students</a> | <a
                            href="team_index.html">Teams</a> | <a
                            href="instructor_index.html"
                            >Instructors</a>
                    </p>
                </div>

                <div class="content">
                    <h1>Teams</h1>
                    <br/>
                    <br/>
                    <table>
                        <thead>
                            <tr>
                                <th class="left_col">Team</th>
                                <th class="left_col">Semester</th>
                                <th class="left_col">Team Id</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/course/teams/team">
                                <xsl:sort select="name"/>
                                <xsl:variable name="current"
                                   select="lower-case(concat('teams/', @teamId, '.html'))"/>
                                <tr>
                                   <td class="name_link">
                                   <a href="{$current}">
                                   <xsl:value-of select="name"/>
                                   </a>
                                   </td>

                                   <td>
                                   <xsl:value-of select="semester"/>
                                   </td>
                                    <td>
                                        <xsl:value-of select="@teamId"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>

                </div>
            </body>
        </html>

    </xsl:template>


    <xsl:template name="generate_individual_pages">

        <xsl:for-each select="/course/teams/team">
            <xsl:variable name="file"
                select="lower-case(concat('teams/', @teamId, '.html'))"/>
            <xsl:result-document href="{$file}">

                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>
                            <xsl:value-of select="name"/>
                        </title>
                        <meta http-equiv="pragma" content="no-cache"/>
                        <meta http-equiv="expires" content="-1"/>
                        <meta http-equiv="cache-control"
                            content="no-cache"/>
                        <meta http-equiv="content-type"
                            content="text/html; charset=utf-8"/>
                        <link rel="stylesheet" type="text/css"
                            href="../css/course_info.css"/>
                    </head>
                    <body onunload="">

                        <xsl:call-template name="common_title_div"/>

                        <div class="navigation">
                            <p>
                                <a href="../index.html">Students</a> |
                                   <a href="../team_index.html"
                                   >Teams</a> | <a
                                   href="../instructor_index.html"
                                   >Instructors</a>
                            </p>
                        </div>

                        <div class="content">

                            <h2>
                                <xsl:value-of select="name"/> (<xsl:value-of select="@teamId"/>)
                            </h2>
                            <table>
                                <thead>
                                    <tr>
                                        <th class="left_col">Team Member</th>
                                        <th class="left_col">e-Mail</th>
                                        <th class="left_col">Web</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>
                                    <tr>
                                        <td>Member Name</td>
                                        <td>email or blank</td>
                                        <td>url or blank</td>
                                    </tr>

                                </tbody>
                            </table>
                            
                            <h2>Client</h2>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Name</th>
                                        <td>name or confidential</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Location</th>
                                        <td>city, state or confidential</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Industry</th>
                                        <td>industry</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Primary Web Site</th>
                                        <td>url or confidential</td>                                        
                                    </tr>
                                </tbody>
                            </table>
                            
                            
                            <h2>Project</h2>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>System Name</th>
                                        <td>name of system</td>                                        
                                    </tr>
                                    <tr>
                                        <th>System Type(s)</th>
                                        <td>Web Site, Mailing List Registration, Quote Request, etc.</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Technical Architecture</th>
                                        <td>technology stack, software tools, etc.</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Project URL</th>
                                        <td>url, confidential, not applicable</td>                                        
                                    </tr>
                                    <tr>
                                        <th>System Demo Video Link</th>
                                        <td>url</td>                                        
                                    </tr>
                                    <tr>
                                        <th>Additional Resource</th>
                                        <td>url</td>                                        
                                    </tr>

                                </tbody>
                            </table>
                            

                        </div>
                    </body>
                </html>

            </xsl:result-document>
        </xsl:for-each>




    </xsl:template>

    <xsl:template name="common_title_div">
        <div class="title">
            <h2>
                <xsl:value-of select="/course/metadata/semesterName"/>
            </h2>
            <h2>
                <xsl:value-of select="/course/metadata/courseNumber"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="/course/metadata/courseName"/>
            </h2>
            <h2>Shared Web Content</h2>
        </div>
    </xsl:template>

</xsl:stylesheet>
