<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xs"
    version="2.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
        scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Jan 10, 2010</xd:p>
            <xd:p><xd:b>Author:</xd:b> Trainor</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>

    <xsl:output
        doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
        method="xml" indent="yes"/>

    <xsl:template match="/">

        <xsl:call-template name="generate_index_page"/>
        <xsl:call-template name="generate_individual_pages"/>

    </xsl:template>

    <xsl:template name="generate_index_page">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>
                    Instructors Index
                </title>
                <meta http-equiv="pragma" content="no-cache"/>
                <meta http-equiv="expires" content="-1"/>
                <meta http-equiv="cache-control" content="no-cache"/>
                <meta http-equiv="content-type"
                    content="text/html; charset=utf-8"/>
                <link rel="stylesheet" type="text/css"
                    href="css/course_info.css"/>
            </head>
            <body onunload="">
                
                <xsl:call-template name="common_title_div"/>
                
                <div class="navigation">
                    <p>
                        <a href="index.html">Students</a> | 
                        <a href="team_index.html">Teams</a> | 
                        <a href="instructor_index.html">Instructors</a>
                        
                    </p>
                </div>  

                <div class="content">
                    <h1>Instructors</h1>
                    <br/>
                    <br/>
                    <table>
                        <thead>
                            <tr>
                                <th class="left_col">Instructor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/course/instructors/instructor">
                                <xsl:sort select="lastName"/>
                                <xsl:sort select="firstName"/>
                                <xsl:variable name="current"
                                    select="lower-case(concat('instructors/', userName, '.html'))"/>
                                <tr>
                                   <td class="name_link">
                                   <a href="{$current}">
                                   <xsl:value-of select="lastName"/>
                                   <xsl:text>, </xsl:text>
                                   <xsl:value-of select="firstName"/>
                                   </a>
                                   </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>

                </div>
            </body>
        </html>

    </xsl:template>


    <xsl:template name="generate_individual_pages">

        <xsl:for-each select="/course/instructors/instructor">
            <xsl:variable name="file"
                select="lower-case(concat('instructors/', userName, '.html'))"/>
            <xsl:result-document href="{$file}">

                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>
                            <xsl:value-of
                                select="firstName"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of
                                select="lastName"/>
                        </title>
                        <meta http-equiv="pragma" content="no-cache"/>
                        <meta http-equiv="expires" content="-1"/>
                        <meta http-equiv="cache-control"
                            content="no-cache"/>
                        <meta http-equiv="content-type"
                            content="text/html; charset=utf-8"/>
                        <link rel="stylesheet" type="text/css"
                            href="../css/course_info.css"/>
                    </head>
                    <body onunload="">
                                               
                        <xsl:call-template name="common_title_div"/>
                        
                        <div class="navigation">
                            <p>
                                <a href="../index.html">Students</a> | 
                                <a href="../team_index.html">Teams</a> | 
                                <a href="../instructor_index.html">Instructors</a>
                                
                            </p>
                        </div>  
                                                
                        <div class="content">

                        <h2>
                            <xsl:value-of select="firstName"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="lastName"/>
                        </h2>
                            <h3><xsl:value-of select="role"/></h3>
                        <p>User Name is <xsl:value-of select="userName"/>.</p>
                        <p><i>Each instructor is expected to add some appropriate content to this part of the page.</i></p>   
                            
                        </div>
                    </body>
                </html>

            </xsl:result-document>
        </xsl:for-each>




    </xsl:template>
    
    <xsl:template name="common_title_div">
        <div class="title">
            <h2><xsl:value-of
                select="/course/metadata/semesterName"
            /></h2>
            <h2>
                <xsl:value-of
                    select="/course/metadata/courseNumber"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of
                    select="/course/metadata/courseName"
                />
            </h2>
            <h2>Shared Web Content</h2>
        </div>
    </xsl:template>

</xsl:stylesheet>
